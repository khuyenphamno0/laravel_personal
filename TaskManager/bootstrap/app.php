<?php

use App\Http\Middleware\NotLoginMiddleware;
use App\Http\Middleware\UserLoginMiddleware;
use Illuminate\Foundation\Application;
use Illuminate\Foundation\Configuration\Exceptions;
use Illuminate\Foundation\Configuration\Middleware;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

return Application::configure(basePath: dirname(__DIR__))
    ->withRouting(
        web: __DIR__.'/../routes/web.php',
        api: __DIR__.'/../routes/api.php',
        commands: __DIR__.'/../routes/console.php',
        health: '/up',
    )
    ->withMiddleware(function (Middleware $middleware) {
        $middleware->alias([
            'userLogedin' => UserLoginMiddleware::class,
            'notLogined' => NotLoginMiddleware::class,
        ]);
    })
    ->withExceptions(function (Exceptions $exceptions) {
        $exceptions->render(function (ValidationException $exception) {
            return response()->json([
                'status_code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'message' => $exception->getMessage(),
                'errors' => $exception->errors(),
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        });
        $exceptions->render(function (NotFoundHttpException $exception) {
            return response()->json([
                'status_code' => Response::HTTP_NOT_FOUND,
                'message' => $exception->getMessage(),
            ], Response::HTTP_NOT_FOUND);
        });
        $exceptions->render(function (MethodNotAllowedHttpException $exception) {
            return response()->json([
                'status_code' => Response::HTTP_METHOD_NOT_ALLOWED,
                'message' => $exception->getMessage(),
            ], Response::HTTP_METHOD_NOT_ALLOWED);
        });
    })->create();
