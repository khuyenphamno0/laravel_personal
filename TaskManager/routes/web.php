<?php

use App\Http\Controllers\Web\User\WebAuthenticationController;
use App\Http\Controllers\Web\Base\BaseController;
use App\Http\Controllers\Web\Task\TaskController;
use App\Http\Middleware\UserLoginMiddleware;
use Illuminate\Support\Facades\Route;
Route::get('/', [BaseController::class,'index'])->name('home');
Route::group([
    'middleware' => ['userLogedin']
], function() {
    Route::prefix('/tasks')->group(function() {
        Route::get('/dashboard',[TaskController::class, 'index']);
        Route::get('/create',[TaskController::class, 'create']);
        Route::get('/edit/{id}',[TaskController::class, 'edit']);
    });
    Route::get('/logout', [WebAuthenticationController::class,'logout']);
});
//Route which Logined User not allowed to access
Route::group([
    'middleware' => ['notLogined']
], function() {
    Route::get('/login', [BaseController::class,'viewlogin'])->name('login');    
    Route::post('/login', [WebAuthenticationController::class, 'login']);
    Route::get('/register', [BaseController::class,'register'])->name('register'); 
});
