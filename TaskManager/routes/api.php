<?php

use App\Http\Controllers\API\Task\TaskController;
use App\Http\Controllers\API\User\AuthenticationController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Route::get('/user', function (Request $request) {
//     return $request->user();
// })->middleware('auth:sanctum');

//Opened Route:
Route::post('/login',[AuthenticationController::class, 'login']);
Route::post('/register',[AuthenticationController::class, 'register']);

//Protected Route:
Route::group([
    'middleware' => ['auth:sanctum']
], function() {
    Route::get('/login/profile',[AuthenticationController::class,'getProfile']);
    Route::get('/logout',[AuthenticationController::class,'logout']);
    Route::apiResource('/tasks', TaskController::class);
});