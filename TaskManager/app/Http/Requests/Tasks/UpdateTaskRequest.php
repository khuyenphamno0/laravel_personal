<?php

namespace App\Http\Requests\Tasks;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        if ($this->method() === 'PUT') {
            return [
                'name' => 'required',
                'limit_time' => 'required|numeric'
            ];
        } elseif ($this->method() === 'PATCH') {
            return [
                'name' => 'sometimes|required',
                'limit_time' => 'sometimes|required|numeric|min:0'
            ];
        }
        
    }
    public function messages(): array
    {
        return [
            'name.required' => 'Please Insert Task Name',
            'limit_time.required' => 'Please Insert Task Limit Time',
            'limit_time.numeric' => 'Task Limit Time must be numeric',
            'limit_time.min' => 'Task Limit Time must be at least 0'
        ];
    }
}
