<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'name' => 'required|string',
            'repeatpassword' => 'required| same:password'
        ];
    }
    public function messages(): array
    {
        return [
            'email.required' => 'Please Insert Email',
            'email.email' => 'Email is Invalid',
            'password.required' => 'Please Insert Password',
            'name.required' => 'Please Insert Name',
            'repeatpassword.required' => 'Please Insert Repeat Password',
            'repeatpassword.same' => 'Repeat Password must be the same with Password'
        ];
    }
}
