<?php

namespace App\Http\Controllers;

class Controller
{
    public function returnResponse($status_code, $message = '', $data)
    {
        return response()->json([
            'status_code' => $status_code,
            'message' => $message,
            'data' => $data
        ], $status_code);
    }
    public function returnSuccessWithToken($status_code = 200, $message = 'Successfull', $token ='', $data)
    {
        return response()->json([
            'status_code' => $status_code,
            'message' => $message,
            'token' => $token,
            'data' => $data
        ], $status_code);
    }
}
