<?php

namespace App\Http\Controllers\API\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\LoginRequest;
use App\Http\Requests\User\RegisterRequest;
use App\Http\Services\User\AuthenticationService;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthenticationController extends Controller
{
    protected $au_service;
    public function __construct(AuthenticationService $au_Service) {
        $this->au_service = $au_Service;
    }
    //Login POST[email,password]
    public function login(LoginRequest $request)
    {
        $dataLogin = [
            'email' => $request->email,
            'password'=> $request->password,
        ];
        //If User is Exists
        $check_login = $this->au_service->login($dataLogin);
        if ($check_login === null) {
            return $this->returnResponse(Response::HTTP_NOT_FOUND, 'Email is Invalid', $dataLogin);
        }

        if (!$check_login) {
            return $this->returnResponse(Response::HTTP_UNAUTHORIZED, 'Invalid Password', $check_login);
        }

        $token = $check_login->createToken('userToken')->plainTextToken;
        return $this->returnSuccessWithToken(Response::HTTP_OK, 'Login Successfull', $token, $check_login);
    }
    //Register POST[name,email,password]
    public function register(RegisterRequest $request)
    {
        $user = $this->au_service->register($request->input());
        if (!$user) {
            return $this->returnResponse(Response::HTTP_BAD_REQUEST, 'Register Failure', $request->input());
        }
        
        return $this->returnResponse(Response::HTTP_CREATED, 'Register Successfull', $user);
    }
    
    //Get Profile POST[Auth:token]
    public function getProfile()
    {
        $user_data = auth()->user();
        return $this->returnResponse(Response::HTTP_OK,'Authorizted', $user_data);
    }

    //Logout POST[Auth::token]
    public function logout()
    {
        auth()->user()->currentAccessToken()->delete();
        return $this->returnResponse(Response::HTTP_OK, 'Logouted Successfull', null);
    }
}
