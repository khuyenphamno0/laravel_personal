<?php

namespace App\Http\Controllers\API\Task;

use App\Http\Controllers\Controller;
use App\Http\Requests\Tasks\CreateTaskRequest;
use App\Http\Requests\Tasks\UpdateTaskRequest;
use App\Http\Resources\Task\TaskResource;
use App\Http\Resources\Task\TaskResourceCollection;
use App\Http\Services\Tasks\TaskService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    protected $task_service;
    public function __construct(TaskService $task_service)
    {
        $this->task_service = $task_service;
    }

    public function index(Request $request)
    {
        if($request->has('searchString')) {
            $tasks = $this->task_service->search($request->searchString);
            $tasks_collection = new TaskResourceCollection($tasks);
            return $this->returnResponse(Response::HTTP_OK, 'Search Successfull' ,$tasks_collection);
        }
        $tasks = $this->task_service->index();
        $tasks_collection = new TaskResourceCollection($tasks);
        return $this->returnResponse(Response::HTTP_OK, 'Get List Successfull', $tasks_collection);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateTaskRequest $request)
    {
        $data = $request->all();
        $task_resource = new TaskResource($this->task_service->store($data));
        return $this->returnResponse(Response::HTTP_CREATED, 'Created Task Successfull', $task_resource);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $task = $this->task_service->find($id);
        $taskResource = new TaskResource($task);
        return $this->returnResponse(Response::HTTP_OK, 'Task Found Successfull', $taskResource);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateTaskRequest $request, string $id)
    {
        $data_input = $request->all();
        $task = $this->task_service->update($id, $data_input);
        $taskResource = new TaskResource($task);
        return $this->returnResponse(Response::HTTP_OK, 'Update Task Successfull', $taskResource);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->task_service->destroy($id);
        return response()->json([
            'status_code' => Response::HTTP_OK,
            'message' => 'Delete Task Successfull',
        ], Response::HTTP_OK);
    }
}
