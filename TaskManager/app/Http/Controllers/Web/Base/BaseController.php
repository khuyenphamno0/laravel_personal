<?php

namespace App\Http\Controllers\Web\Base;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BaseController extends Controller
{
    public function viewLogin()
    {   
        return view('login');
    }
    public function register() 
    {
        return view('register');
    }
    public function index()
    {
        return view('welcome');
    }
}
