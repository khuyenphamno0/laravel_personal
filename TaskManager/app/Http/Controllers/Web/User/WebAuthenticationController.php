<?php

namespace App\Http\Controllers\Web\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\LoginRequest;
use App\Http\Services\User\AuthenticationService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class WebAuthenticationController extends Controller
{
    protected $au_service;
    public function __construct(AuthenticationService $au_service)
    {
        $this->au_service = $au_service;
    }
    public function login(LoginRequest $request)
    {
        $dataLogin = [
            'email'=> $request->email,
            'password'=> $request->password
        ];
        $check_login = $this->au_service->login($dataLogin);
        if ($check_login === null) {
            return $this->returnResponse(Response::HTTP_NOT_FOUND, 'Email is Invalid', $dataLogin);
        }

        if (!$check_login) {
            return $this->returnResponse(Response::HTTP_UNAUTHORIZED, 'Invalid Password', $check_login);
        }
        
        return $this->returnResponse(Response::HTTP_OK,'Login Successfull', $check_login);
    }

    public function logout() 
    {
        try {
            Auth::logout();
            return response()->json([
                'status_code' => 200
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'status_code' => 403,
            ]);
        }
    }
}
