<?php

namespace App\Http\Controllers\Web\Task;

use App\Http\Controllers\Controller;
use App\Http\Services\Tasks\TaskService;
use Illuminate\Http\Request;

class TaskController extends Controller
{
    protected $task_service;
    public function __construct(TaskService $task_service)
    {
        $this->task_service = $task_service;
    }
    public function index()
    {
        return view('Task.dashboard');
    }
    
    public function create()
    {
        return view('Task.create');
    }

    public function edit($id) 
    {
        $task = $this->task_service->find($id);
        return view('Task.edit', compact('task'));
    }
}
