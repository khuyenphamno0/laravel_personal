<?php

namespace App\Http\Resources\Task;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;

class TaskResourceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @return array<int|string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'Tasks' => $this->collection,
            'meta' => [
                'total' => $this->total(),
                'links' => [
                    'first' => $this->url(1),
                    'last' => $this->url($this->lastPage()),
                    'next' => $this->nextPageUrl(),
                    'previous' => $this->previousPageUrl()
                ],
                'totalPage' => $this->lastPage()
            ]
        ];
    }
}
