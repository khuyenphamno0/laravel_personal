<?php

namespace App\Http\Services\User;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthenticationService
{
    public function login($data_login)
    {
        $user = User::where('email', $data_login)->first();
        //check if user is exists
        if (!empty($user)) {
            //Check if password is correct and login
            if (Auth::attempt($data_login)) {
                return $user;
            }
            return false;
        }
        return null;
    }
    
    public function logout()
    {
        $user = Auth::user();
        Auth::logout();
        return true;
    }

    public function register($data) {
        $hashed_password = Hash::make($data['password']);
        try {
            $user = User::create($data);
            return $user;
        } catch (\Throwable $th) {
            return false;
        }
    }
}