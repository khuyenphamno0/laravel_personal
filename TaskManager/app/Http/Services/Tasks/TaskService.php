<?php
 
 namespace App\Http\Services\Tasks;

use App\Models\Tasks;

 class TaskService
 {
    protected $task;
    public function __construct(Tasks $task)
    {
        $this->task = $task;
    }
    
    public function index()
    {
        return $this->task->paginate(7);
    }

    public function find($id)
    {
        return $this->task->findOrFail($id);
    }
    
    public function store($data)
    {
        try {
            return $this->task->create($data);
        } catch (\Throwable $th) {
            return false;
        }
    }

    public function update($id, $data)
    {
        $task = $this->task->findOrFail($id);
        try {
            $task->update($data);
            return $task;
        } catch (\Throwable $th) {
            return false;
        }
    }
    
    public function destroy($id)
    {
        $task = $this->task->findOrFail($id);
        $task->delete();
        return true;
    }

    public function search($searchString)
    {
        $tasks = $this->task->where('name', 'LIKE', "%{$searchString}%")->paginate(7);
        return $tasks;
    }
 }