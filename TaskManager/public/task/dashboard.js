$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajaxSetup({
        beforeSend: function(xhr) {
            var token = localStorage.getItem('token');
            if (token) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + token);
            }
        }
    });
    getListTask(1)
})

function logout() {
    $.ajax({
        type: 'GET',
        url: '/logout',
        success: function(response) {
            if (response.status_code === 200) {
                location.href = '/'
            }
        }
    })
    $.ajax({
        type: 'GET',
        url: '/api/logout',
        success: function(response) {
            if (response.status_code === 200) {
                localStorage.removeItem('token')
            }
        }
    })
}

function appendTable(tasks) {
    document.getElementById('listTask').innerHTML = ''
    tasks.forEach(task => {
        $('#listTask').append(
            '<tr id="'+task.id+ '">\
            <td>'+task.id+'</td>\
            <td>'+task.name+'</td>\
            <td>'+task.limit_time+'</td>\
            <td>'+task.description+'</td>\
            <td>\
                <a href="/tasks/edit/'+task.id+'"><button class="btn btn-edit">Edit</button></a>\
                <button onclick="deleteTask('+task.id+')" class="btn btn-delete">Delete</button>\
            </td>\
            </tr>\
            '
        )
    });
}

function getListTask(page) {
    $.ajax({
        type: 'GET',
        dataType: 'JSON',
        url: '/api/tasks/?page=' +page,
        success: function(response) {
            var tasks = response.data.Tasks
            document.getElementById('currentPage').innerHTML = page
            document.getElementById('totalPage').innerHTML = response.data.meta.totalPage
            appendTable(tasks)
        }
    })
}

function nextPage(){
    var currentPage = parseInt(document.getElementById('currentPage').textContent)
    var totalPage = parseInt(document.getElementById('totalPage').textContent)
    if (currentPage < totalPage) {
        getListTask(currentPage+1)
    }
}

function prevPage(){
    var currentPage = parseInt(document.getElementById('currentPage').textContent)
    if (currentPage > 1) {
        getListTask(currentPage-1)
    }
}

function deleteTask(id) {
    Swal.fire({
        title: 'Are You Sure?',
        text: 'You wont be able to Undo this!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: 'DELETE',
                url: '/api/tasks/' + id,
                dataType: 'JSON',
                success: function(response) {
                    if (response.status_code === 200) {
                        Swal.fire(
                            'Successfully',
                            response.message,
                           'success'
                        ).then(function() {
                            location.reload();
                        })
                    }
                },
                error: function(response) {
                    var error = JSON.parse(response.responseText)
                    Swal.fire(
                        'Failure',
                        error.message,
                        'error'
                    )
                }
            })
        }
    })
}

function searchTask() {
    var searchData = {
        'searchString' :  $('#searchTask').val()
    }
    $.ajax({
        type: 'GET',
        dataType: 'JSON',
        url: '/api/tasks',
        data: searchData,
        success: function(response) {
            var tasks = response.data.Tasks
            document.getElementById('currentPage').innerHTML = 1
            document.getElementById('totalPage').innerHTML = response.data.meta.totalPage
            appendTable(tasks)
        }
    })
}
