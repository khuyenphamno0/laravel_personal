$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajaxSetup({
        beforeSend: function(xhr) {
            var token = localStorage.getItem('token');
            if (token) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + token);
            }
        }
    });
})

function functionLogout() {
    $.ajax({
        type: 'GET',
        url: '/logout',
        success: function(response) {
            if (response.status_code === 200) {
                location.href = '/'
            }
        }
    })
    $.ajax({
        type: 'GET',
        url: '/api/logout',
        success: function(response) {
            if (response.status_code === 200) {
                localStorage.removeItem('token')
            }
        }
    })
}
//Check if inputs is being changed and set button do able

$(document).on('keyup', '#name', function() {
    document.getElementById('updateButton').disabled = false
})


$(document).on('change', '#limit_time', function() {
    document.getElementById('updateButton').disabled = false
})


$(document).on('keyup', '#description', function() {
    document.getElementById('updateButton').disabled = false
})

function sendUpdateRequest(id)
{
    var data = {
        'name' : $('#name').val(),
        'limit_time' : $('#limit_time').val(),
        'description' : $('#description').val(),
    }
    $.ajax({
        type: 'PATCH',
        url: '/api/tasks/' + id,
        data: data,
        dataType: 'JSON',
        success: function(response) {
            if (response.status_code === 200) {
                Swal.fire(
                    'Successfull',
                    response.message,
                   'success'
                ).then(function() {
                    location.reload();
                })
            }
        },
        error: function(response) {
            var error = JSON.parse(response.responseText)
            Swal.fire(
                'Failure',
                error.message,
                'error'
            )
        }
    })
}