$(document).ready(function() {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajaxSetup({
        beforeSend: function(xhr) {
            var token = localStorage.getItem('token');
            if (token) {
                xhr.setRequestHeader('Authorization', 'Bearer ' + token);
            }
        }
    });
})

function functionLogout() {
    $.ajax({
        type: 'GET',
        url: '/logout',
        success: function(response) {
            if (response.status_code === 200) {
                location.href = '/'
            }
        }
    })
    $.ajax({
        type: 'GET',
        url: '/api/logout',
        success: function(response) {
            if (response.status_code === 200) {
                localStorage.removeItem('token')
            }
        }
    })
}

function sendCreateRequest() {
    var data = {
        'name' : $('#name').val(),
        'limit_time' : $('#limit_time').val(),
        'description' : $('#description').val(),
    }
    $.ajax({
        type: 'POST',
        data: data,
        dataType: 'JSON',
        url: '/api/tasks',
        success: function(response) {
            if (response.status_code === 201) {
                Swal.fire(
                    'Successfull',
                    response.message,
                    'success'
                ).then(function() {
                    location.href = '/tasks/dashboard'
                })
            }
        },
        error: function(response) {
            var error = JSON.parse(response.responseText)
            Swal.fire(
                'Failure',
                error.message,
                'error'
            )
        }
    })
}