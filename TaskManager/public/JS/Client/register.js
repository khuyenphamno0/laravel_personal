$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
function sendRegisterRequest() {
    var data = {
        'name' : $('#name').val(),
        'email' : $('#email').val(),
        'password' : $('#password').val(),
        'repeatpassword' : $('#repeatpassword').val()
    }
    $.ajax({
        type: 'POST',
        data: data,
        dataType: 'json',
        url: '/api/register',
        success: function(response) {
            if (response.status_code === 201) {
                Swal.fire(
                    'Successfull',
                    response.message,
                    'success'
                ).then(function() {
                    location.href = '/login'
                })
            }
        },
        error: function(response) {
            var error = JSON.parse(response.responseText)
            document.getElementById('error').innerHTML = error.message
        }
    })
}