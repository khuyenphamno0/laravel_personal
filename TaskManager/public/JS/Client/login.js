$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
// Load page
function loginFunction(){
    var email = $('#email').val()
    var password = $('#password').val()
    var datainput = {
        'email' : email,
        'password' : password
    }
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/api/login',
        data: datainput,
        success: function(response) {
            localStorage.setItem('token', response.token)
        },
        error: function(response) {
            var error = JSON.parse(response.responseText)
            document.getElementById('error').innerHTML = error.message
        }
    })
    $.ajax({
        type: 'POST',
        dataType: 'json',
        url: '/login',
        data: datainput,
        success: function(response) {
            if (response.status_code === 200) {
                location.href = '/tasks/dashboard'
            }
        },
    })
}