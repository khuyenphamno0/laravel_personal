<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class GetListTaskTest extends TestCase
{
   
    public function listTaskUrl()
    {
        return '/api/tasks';
    }
    public function userToLogin()
    {
        return User::find(1);
    }
     /** @test */
    public function user_logined_can_get_list_task(): void
    {
        $user = $this->userToLogin();

        Sanctum::actingAs($user);
        $response = $this->getJson($this->listTaskUrl());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJsonStructure([
            'status_code',
            'message',
            'data' => [
                'Tasks' => [
                    '*' => [
                        'id',
                        'name',
                        'limit_time',
                        'description',
                    ]
                    ],
                'meta' => [
                    'total',
                    'links' => [
                        'first',
                        'last',
                        'next',
                        'previous'
                    ],
                    'totalPage'
                ] 
            ]
        ]);
    }
    /** @test */
    public function user_not_logined_cant_get_list_tasks(): void 
    {
        $response = $this->getJson($this->listTaskUrl());
        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
        $response->assertExactJson([
            'message' => 'Unauthenticated.'
        ]);
    }
}
