<?php

namespace Tests\Feature\Tasks;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class CreateTaskTest extends TestCase
{
    public function userToLogin()
    {
        return User::find(1);
    }
    public function responseUrl()
    {
        return '/api/tasks';
    }
    /** @test */
    public function user_logined_can_create_task()
    {
        $data = [
            'name' => fake()->name(),
            'limit_time' => fake()->numberBetween(1,100),
            'description' => fake()->sentence()
        ];
        Sanctum::actingAs($this->userToLogin());
        $response = $this->postJson($this->responseUrl(), $data);
        $response->assertStatus(Response::HTTP_CREATED)
        ->assertJson([
            'status_code' => Response::HTTP_CREATED,
            'message' => 'Created Task Successfull',
            'data' => [
                'name' => $data['name'],
                'limit_time' => $data['limit_time'],
                'description' => $data['description']
            ]
        ]);
    }

    /** @test */
    public function user_logined_cant_create_task_not_send_name()
    {
        $data = [
            'limit_time' => fake()->numberBetween(1,100),
            'description' => fake()->sentence()
        ];
        Sanctum::actingAs($this->userToLogin());
        $response = $this->postJson($this->responseUrl(), $data);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
        ->assertExactJson([
            'status_code' => Response::HTTP_UNPROCESSABLE_ENTITY,
            'message' => 'Please Insert Task Name',
            'errors' => [
                'name' => [
                    'Please Insert Task Name'
                ]
            ]
        ]);
    }
    /** @test */
    public function user_logined_cant_create_task_not_send_limit_time()
    {
        $data = [
            'name' => fake()->name(),
            'description' => fake()->sentence()
        ];
        $user = $this->userToLogin();
        Sanctum::actingAs($user);
        $response = $this->postJson($this->responseUrl(), $data);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
        ->assertExactJson([
            'status_code' => Response::HTTP_UNPROCESSABLE_ENTITY,
            'message' => 'Please Insert Task Limit Time',
            'errors' => [
                'limit_time' => [
                    'Please Insert Task Limit Time'
                ]
            ]
        ]);
    }
    
    /** @test */
    public function user_logined_cant_create_task_send_invalid_limit_time()
    {
        $data = [
            'name' => fake()->name(),
            'limit_time' => 'a',
            'description' => fake()->sentence()
        ];
        $user = $this->userToLogin();
        Sanctum::actingAs($user);
        $response = $this->postJson($this->responseUrl(), $data);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
        ->assertExactJson([
            'status_code' => Response::HTTP_UNPROCESSABLE_ENTITY,
            'message' => 'Task Limit Time must be numeric',
            'errors' => [
                'limit_time' => [
                    'Task Limit Time must be numeric'
                ]
            ]
        ]);
    }

    /** @test */
    public function user_not_logined_cant_create_task_()
    {
        $data = [
            'name' => fake()->name(),
            'limit_time' => 'a',
            'description' => fake()->sentence()
        ];
        $response = $this->postJson($this->responseUrl(), $data);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED)
        ->assertExactJson([
            'message' => 'Unauthenticated.'
        ]);
    }
}
