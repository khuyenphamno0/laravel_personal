<?php

namespace Tests\Feature\Tasks;

use App\Models\Tasks;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class Get1TaskTest extends TestCase
{
    public function responseUrl($id) 
    {
        return '/api/tasks/'.$id;
    }
    public function userToLogin()
    {
        return User::find(1);
    }
    /** @test */
    public function user_logined_can_get_1_task(): void
    {
        Sanctum::actingAs($this->userToLogin());
        $task = Tasks::factory()->create();
        $id = $task->id;
        $response = $this->getJson($this->responseUrl($id));
        $response->assertStatus(Response::HTTP_OK)
        ->assertExactJson([
            'status_code' => Response::HTTP_OK,
            'message' => 'Task Found Successfull',
            'data' => [
                'id' => $task->id,
                'name' => $task->name,
                'limit_time' => $task->limit_time,
                'description' => $task->description
            ]
        ]);   
    }

     /** @test */
    public function user_logined_cant_get_1_task_not_found(): void
    {
        Sanctum::actingAs($this->userToLogin());
        $id = -1;
        $response = $this->getJson($this->responseUrl($id));
        $response->assertStatus(Response::HTTP_NOT_FOUND)
        ->assertExactJson([
            'status_code' => Response::HTTP_NOT_FOUND,
            'message' => 'No query results for model [App\\Models\\Tasks] -1',
        ]);   
    }

    /** @test */
    public function user_not_logined_cant_get_1_task(): void
    {
        $task = Tasks::factory()->create();
        $id = $task->id;
        $response = $this->getJson($this->responseUrl($id));
        $response->assertStatus(Response::HTTP_UNAUTHORIZED)
        ->assertExactJson([
           'message' => 'Unauthenticated.'
        ]);
    }

}
