<?php

namespace Tests\Feature\Tasks;

use App\Models\Tasks;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{
    public function userToLogin()
    {
        return User::find(1);
    }
    public function responseUrl($id)
    {
        return '/api/tasks/'.$id;
    }

    /** @test */
    public function user_logined_can_update_task_with_put()
    {
        $user = $this->userToLogin();
        $task = Tasks::factory()->create([]);
        //prepare data to put
        $data_to_put = [
            'name' => 'Task Name has been updated',
            'limit_time' => $task->limit_time,
            'description' => $task->description
        ];
        //login 
        Sanctum::actingAs($user);

        $response = $this->putJson($this->responseUrl($task->id), $data_to_put);
        $response->assertStatus(Response::HTTP_OK)
        ->assertExactJson([
            'status_code' => Response::HTTP_OK,
            'message' => 'Update Task Successfull',
            'data' => [
                'id' => $task->id,
                'name' => $data_to_put['name'],
                'limit_time' => $task->limit_time,
                'description' => $task->description
            ]
        ]);
    }

    /** @test */
    public function user_logined_cant_update_task_not_found_with_put()
    {
        $user = $this->userToLogin();
        //prepare data to put
        $data_to_put = [
            'name' => 'Task Name has been updated',
            'limit_time' => 15,
            'description' => fake()->sentence()
        ];
        //login 
        Sanctum::actingAs($user);

        $response = $this->putJson($this->responseUrl(-1), $data_to_put);
        $response->assertStatus(Response::HTTP_NOT_FOUND)
        ->assertExactJson([
            'status_code' => Response::HTTP_NOT_FOUND,
            'message' => 'No query results for model [App\\Models\\Tasks] -1',
        ]);
    }

    /** @test */
    public function user_logined_cant_update_task_with_put_send_invalid_input()
    {
        $user = $this->userToLogin();
        $task = Tasks::factory()->create();
        //prepare data to put
        $data_to_put = [
            'name' => 'Task Name has been updated',
            //Limit time must be number
            'limit_time' => 'aa',
            'description' => fake()->sentence()
        ];
        //login 
        Sanctum::actingAs($user);

        $response = $this->putJson($this->responseUrl($task->id), $data_to_put);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
        ->assertExactJson([
            'status_code' => Response::HTTP_UNPROCESSABLE_ENTITY,
            'message' => 'Task Limit Time must be numeric',
            'errors' => [
                'limit_time' => [
                    'Task Limit Time must be numeric'
                ]
            ]
        ]);
    }


    /** @test */
    public function user_logined_cant_update_task_with_put_not_send_task_name()
    {
        $user = $this->userToLogin();
        $task = Tasks::factory()->create();
        //prepare data to put
        $data_to_put = [
            'limit_time' => 18,
            'description' => fake()->sentence()
        ];
        //login 
        Sanctum::actingAs($user);

        $response = $this->putJson($this->responseUrl($task->id), $data_to_put);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
        ->assertExactJson([
            'status_code' => Response::HTTP_UNPROCESSABLE_ENTITY,
            'message' => 'Please Insert Task Name',
            'errors' => [
                'name' => [
                    'Please Insert Task Name'
                ]
            ]
        ]);
    }

    /** @test */
    public function user_not_logined_cant_update_task_with_put()
    {
        $task = Tasks::factory()->create([]);
        //prepare data to put
        $data_to_put = [
            'name' => 'Task Name has been updated',
            'limit_time' => $task->limit_time,
            'description' => $task->description
        ];

        $response = $this->putJson($this->responseUrl($task->id), $data_to_put);
        $response->assertStatus(Response::HTTP_UNAUTHORIZED)
        ->assertExactJson([
            'message' => 'Unauthenticated.'
        ]);
    }

    /** @test */
    public function user_logined_can_update_task_with_patch_not_send_limit_time()
    {
        $user = $this->userToLogin();
        $task = Tasks::factory()->create([]);
        //prepare data to put
        $data_to_put = [
            'name' => 'Task Name has been updated',
            'description' => $task->description
        ];
        //login 
        Sanctum::actingAs($user);

        $response = $this->patchJson($this->responseUrl($task->id), $data_to_put);
        $response->assertStatus(Response::HTTP_OK)
        ->assertExactJson([
            'status_code' => Response::HTTP_OK,
            'message' => 'Update Task Successfull',
            'data' => [
                'id' => $task->id,
                'name' => $data_to_put['name'],
                'limit_time' => $task->limit_time,
                'description' => $task->description
            ]
        ]);
    }


}
