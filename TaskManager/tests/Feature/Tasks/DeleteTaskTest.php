<?php

namespace Tests\Feature\Tasks;

use App\Models\Tasks;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Laravel\Sanctum\Sanctum;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{
    public function responseUrl($id) 
    {
        return '/api/tasks/'. $id;
    }
    public function userToLogin()
    {
        return User::find(1);
    }

    /** @test */
    public function user_logined_can_delete_task(): void
    {
        $user = $this->userToLogin();
        Sanctum::actingAs($user);
        $task = Tasks::factory()->create();
        $count = Tasks::count();
        $response = $this->deleteJson($this->responseUrl($task->id));
        $response->assertStatus(Response::HTTP_OK)
        ->assertExactJson([
            'status_code' => Response::HTTP_OK,
            'message' => 'Delete Task Successfull'
        ]);
        $this->assertEquals($count-1, Tasks::count());
    }
    
    /** @test */
    public function user_logined_cant_delete_task_not_found(): void
    {
        $user = $this->userToLogin();
        Sanctum::actingAs($user);
        $response = $this->deleteJson($this->responseUrl(-1));
        $response->assertStatus(Response::HTTP_NOT_FOUND)
        ->assertExactJson([
            'status_code' => Response::HTTP_NOT_FOUND,
            'message' => 'No query results for model [App\\Models\\Tasks] -1'
        ]);
    }

    /** @test */
    public function user_not_logined_cant_delete_task(): void
    {
        $response = $this->deleteJson($this->responseUrl(-1));
        $response->assertStatus(Response::HTTP_UNAUTHORIZED)
        ->assertExactJson([
            'message' => 'Unauthenticated.'
        ]);
    }
}
