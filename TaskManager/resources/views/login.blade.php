@extends('layout.app')

@section('content')
    <div class="login-container">
        <div>
            <h2>Login</h2>
            <h3 id='error' style="color:red"></h3>
        </div>
        
        <form>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" id="email" name="email" required>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" id="password" name="password" required>
            </div>
            <button type="button" onclick="loginFunction()" class="btn">Login</button>
            <div class="forgot-password">
                <a href="/register">Register</a>
            </div>
        </form>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="JS/Client/login.js"></script>
@endsection