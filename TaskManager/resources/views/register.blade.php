@extends('layout.app')

@section('content')
<div class="header">
        <button onclick="window.location.href='/login'">Login</button>
    </div>

    <div class="container">
        <div>
            <h2>Register</h2>
            <h3 id='error' style="color:red"></h3>
        </div>
        <form>
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" id="name" name="name" required>
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" id="email" name="email" required>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" id="password" name="password" required>
            </div>
            <div class="form-group">
                <label for="repeatpassword">Repeat Password</label>
                <input type="password" id="repeatpassword" name="repeatpassword" required>
            </div>
            <div class="form-actions">
                <button type="button" class="btn btn-back" onclick="window.location.href='/'">Back</button>
                <button type="button" onclick="sendRegisterRequest()" class="btn btn-register">Register</button>
            </div>
        </form>
    </div>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script src="JS/Client/register.js"></script>
@endsection