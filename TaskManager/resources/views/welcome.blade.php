<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task Management Home</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            display: flex;
            flex-direction: column;
            height: 100vh;
        }

        .header {
            display: flex;
            justify-content: space-between;
            align-items: center;
            padding: 20px;
            background-color: #f8f9fa;
            border-bottom: 1px solid #dee2e6;
        }

        .header h1 {
            margin: 0;
        }

        .nav-buttons {
            display: flex;
            gap: 10px;
        }

        .btn {
            padding: 10px 20px;
            text-decoration: none;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            color: white;
        }

        .btn-login {
            background-color: #007bff; /* Blue */
        }

        .btn-register {
            background-color: #28a745; /* Green */
        }

        .content {
            flex: 1;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            text-align: center;
            padding: 20px;
        }

        .content h2 {
            font-size: 2em;
            margin-bottom: 20px;
        }

        .content p {
            font-size: 1.2em;
            margin-bottom: 20px;
        }

        .btn-dashboard {
            padding: 10px 20px;
            text-decoration: none;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            background-color: #17a2b8; /* Light blue */
            color: white;
        }

        .info-section {
            text-align: left;
            margin-top: 40px;
        }

        .info-section h3 {
            font-size: 1.5em;
            margin-bottom: 10px;
        }

        .info-section p {
            font-size: 1em;
            line-height: 1.6;
        }
    </style>
</head>
<body>
    <?php

use Illuminate\Support\Facades\Auth;

 if (!Auth::check()){ echo '<div class="header">
        <h1>Task Management</h1>
        <div class="nav-buttons">
            <a href="/login"><button class="btn btn-login">Login</button></a> 
            <a href="/register"><button class="btn btn-register" onclick="gotoRegister()">Register</button></a>
        </div>
    </div>';} ?>
    

    <div class="content">
        <h2>Welcome to the Task Management System</h2>
        <p>This is a simple platform to manage your tasks efficiently and effectively.</p>
        <button class="btn-dashboard" onclick="window.location.href='/tasks/dashboard'">Go to Task Dashboard</button>

        <div class="info-section">
            <h3>What is a Task?</h3>
            <p>A task is a specific piece of work to be done or undertaken. Tasks can range from small, individual activities to large, complex projects. They are essential components of daily work life and personal activities.</p>

            <h3>Why Manage Tasks?</h3>
            <p>Managing tasks is crucial for several reasons:</p>
            <ul>
                <li><strong>Organization:</strong> Keeping tasks organized helps you prioritize and ensure that important tasks are completed on time.</li>
                <li><strong>Productivity:</strong> A well-managed task list can significantly boost your productivity by providing a clear roadmap of what needs to be done.</li>
                <li><strong>Accountability:</strong> Tracking tasks and their completion statuses help in maintaining accountability, both personally and within teams.</li>
                <li><strong>Stress Reduction:</strong> Knowing that your tasks are managed and under control can reduce stress and improve overall well-being.</li>
            </ul>
        </div>
    </div>
</body>
</html>