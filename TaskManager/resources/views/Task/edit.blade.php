<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Create Task</title>
    <style>
        body {
            font-family: 'Arial', sans-serif;
            margin: 0;
            padding: 0;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
            min-height: 100vh;
            background-color: #f0f2f5;
        }
        .container {
            background-color: #fff;
            padding: 30px;
            border-radius: 10px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            width: 400px;
            position: relative;
        }
        h1 {
            text-align: center;
            margin-bottom: 20px;
            font-size: 24px;
            color: #333;
        }
        label {
            display: block;
            margin-bottom: 8px;
            font-weight: bold;
            color: #555;
        }
        input[type="text"], input[type="number"], textarea {
            width: calc(100% - 16px);
            padding: 10px;
            margin-bottom: 20px;
            border: 1px solid #ccc;
            border-radius: 6px;
            font-size: 16px;
            outline: none;
        }
        input[type="text"]:focus, input[type="number"]:focus, textarea:focus {
            border-color: #80bdff;
            box-shadow: 0 0 5px rgba(128, 189, 255, 0.5);
        }
        button {
            padding: 12px 20px;
            background-color: #007bff;
            color: #fff;
            border: none;
            border-radius: 6px;
            font-size: 16px;
            cursor: pointer;
            width: 100%;
        }
        button:hover {
            background-color: #0056b3;
        }
        .top-left, .top-right {
            position: absolute;
            top: 10px;
        }
        .top-left {
            left: 10px;
        }
        .top-right {
            right: 10px;
        }
        .top-right a, .top-left a {
            margin-left: 10px;
            text-decoration: none;
            color: #007bff;
            font-weight: bold;
            font-size: 16px;
        }
        .top-right a:hover, .top-left a:hover {
            text-decoration: underline;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="top-left">
            <a href="/" >
                <button class="btn btn-primary" style="width:100px" >Go home</button>
            </a>
        </div>
        <div class="top-right">
            <a href="/tasks/dashboard"><button class="btn btn-success" style="width:100px" >Back</button></a>
            <button onclick="functionLogout()" class="btn btn-warning" style="width:100px; margin-left: 10px;" >Logout</button>
            
        </div>
        <h1 style="margin-top:40px" >Edit Task: {{$task->name}}</h1>
        <form>
            <label for="name">Task Name</label>
            <input  value="{{$task->name}}" type="text" id="name" name="name" required>
            
            <label for="limit_time">Limit Time (in Minutes)</label>
            <input  type="number" value="{{$task->limit_time}}" id="limit_time" name="limit_time" required>
            
            <label for="description">Description</label>
            <textarea id="description" name="description" rows="4" >{{$task->description}}</textarea>
            
            <button id="updateButton" disabled onclick="sendUpdateRequest('{{$task->id}}')" type="button">Update Task</button>
        </form>
    </div>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="/task/edit.js"></script>