<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Task List</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }

        .header {
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        .logout-btn {
            padding: 10px 20px;
            text-decoration: none;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            background-color: #d9534f; /* Red */
            color: white;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
        }

        table, th, td {
            border: 1px solid black;
        }

        th, td {
            padding: 10px;
            text-align: left;
        }

        th {
            background-color: #f2f2f2;
        }

        .action-buttons {
            display: flex;
            gap: 10px;
        }

        .btn {
            padding: 5px 10px;
            text-decoration: none;
            border: none;
            border-radius: 3px;
            cursor: pointer;
            color: white;
        }

        .btn-edit {
            background-color: #5bc0de; /* Light blue */
        }

        .btn-delete {
            background-color: #d9534f; /* Red */
        }
    </style>
</head>
<body>
    <div class="header">
        <div>
            <h1>Task List</h1>
            <a href="/tasks/create" ><button class="logout-btn" style="margin-right:10px; background-color:#FF7B5A">Create task</button></a> 
            <div>
                <input type="text" id="searchTask" placeholder="Search by task name" style="margin-top: 5px;width: 200px; margin-right: 10px; padding: 5px;" />
                <button onclick="searchTask()" class="logout-btn" style="padding: 5px 10px; background-color:#008080 ;">Search</button>
            </div>
        </div>
        <div style="display:flex">
            <a href="/" ><button class="logout-btn" style="margin-right:10px; background-color:#008080">Go Home</button></a>
            <button class="logout-btn" onclick="logout()">Logout</button>
        </div>
    </div>

    <table>
        <thead>
            <tr>
                <th>ID</th>
                <th>Task Name</th>
                <th>Limit Time</th>
                <th>Task Description</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody id="listTask">
           
            <!-- Thêm các dòng khác tương tự -->
        </tbody>
    </table>
    <div>
        <div style="display:flex; position:absolute">
            <button onclick="prevPage()" style="width: 30px; height:20px; margin-top:15px;margin-right:5px; background-color:#78d166" ><i class="fa fa-arrow-left" ></i></button>
                <div style="display:flex">
                    <h3>Page &nbsp;</h3>
                    <h3 id="currentPage"></h3>
                    <h3>&nbsp; of &nbsp;</h3>
                    <h3 id="totalPage"></h3>
                </div>
                <button onclick="nextPage()" style="width: 30px; height:20px; margin-top:15px;margin-left:5px; background-color:#78d166" ><i class="fa fa-arrow-right" ></i></button>
        </div>
    </div>

</body>
</html>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="/task/dashboard.js"></script>

